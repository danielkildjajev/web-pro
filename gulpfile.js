'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');

//compile
gulp.task('sass', done => {
    gulp.src('assets/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('assets/css'));
    done();
});
//compile and watch
// gulp sass:watch
gulp.task('sass:watch', done => {
    gulp.watch('assets/scss/*.scss',  gulp.series('sass'));
    done();
});
// if gulp command
gulp.task('default', gulp.parallel('sass:watch'));
